# mwoof

Offer one file for download by a web browser.

## Contributors

   * Matt Weidner <jopawp@gmail.com>

## Features

   * Implements a minimal http server.
   * Configurable listen port.
   * Configurable listen interface, defaults to listening on all network interfaces.
   * Test mode to verify network connectivity without needing to transfer a file.
   * Serves one file to multiple clients.
   * Colorized terminal output by default. Toggle with -n switch.

## Requirements

   * mlog gem for printing messages to the console. [https://bitbucket.org/jopawp/mlog-gem](https://bitbucket.org/jopawp/mlog-gem)
   * Requires the Linux shell utility 'file' to determine the appropriate mime-type of the target file.

## Future Enhancements

   * Add support for SSL connections.
   * Add --no-ssl switch to enable SSL on. Default to on.